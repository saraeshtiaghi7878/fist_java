package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String dayOfWeek_String = scanner.nextLine();
        int dayOfWeek = Integer.parseInt(dayOfWeek_String);
        if (dayOfWeek == 1) {
            System.out.println("saturday");
        } else if (dayOfWeek == 2) {
            System.out.println("sunday");
        } else if (dayOfWeek == 3) {
            System.out.println("monday");
        } else if (dayOfWeek == 4) {
            System.out.println("thursday");
        } else if (dayOfWeek == 5) {
            System.out.println("wednesday");
        } else if (dayOfWeek == 6) {
            System.out.println("tuesday");
        } else if (dayOfWeek == 7) {
            System.out.println("friday");
        } else {
            System.out.println("please inter current number");
        }
    }
}